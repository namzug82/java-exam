package com.privalia.repositories;

import com.privalia.domain.Student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.privalia.configuration.RepositoryConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { RepositoryConfiguration.class })
public class StudentRepositoryTest {
	@Autowired
	private StudentRepository studentRepository;
	private Student student1 = null;
	private Student student2 = null;

	@Rule
	public TestName testName = new TestName();

	Logger logger = LoggerFactory.getLogger(StudentRepositoryTest.class);

	@Before
	public void setUp() throws Exception {
		student1 = new Student();
		student1.setName("Pepe");
		student1.setAddress("Balmes");
		studentRepository.save(student1);

		student2 = new Student();
		student2.setName("Manolo");
		student2.setAddress("Diagonal");
		studentRepository.save(student2);
		logger.info("Started test " + testName.getMethodName());
	}

	@After
	public void after() {
		logger.info("Finished test " + testName.getMethodName());
		studentRepository.deleteAll();
	}

	@Test
	public void testSaveStudent() {
		Student student = new Student();
		student.setName("Maria");
		student.setAddress("Aribau");
		assertNull(student.getId());
		studentRepository.save(student);
		assertNotNull(student.getId());
	}

	@Test
	public void testGetAllStudents() {
		Iterable<Student> students = studentRepository.findAll();
		long size = students.spliterator().getExactSizeIfKnown();
		assertEquals(size, 2);
	}

	@Test
	public void testModifyStudent() {
		student1.setName("Pepa");
		Student updatedStudent = studentRepository.save(student1);
		assertEquals(updatedStudent.getName(), "Pepa");
	}

	@Test
	public void testRemoveStudent() {
		studentRepository.delete(student1);
		assertNull(studentRepository.findOne(student1.getId()));
	}

	@Test
	public void testFindByStudentId() {
		Student found = studentRepository.findById(1);
		assertNotNull(found.getId());
		assertEquals(found.getId(), "1");
	}
}