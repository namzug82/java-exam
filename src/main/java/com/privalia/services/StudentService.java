package com.privalia.services;

import com.privalia.domain.Student;

public interface StudentService {
	Iterable<Student> listAllStudents();

	Student getStudentById(Integer id);

	Student saveStudent(Student student);

	void deleteStudent(Integer id);
}
