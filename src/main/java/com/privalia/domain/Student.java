package com.privalia.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "student")
@EntityListeners(AuditingEntityListener.class)
public class Student implements Serializable
{
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    private Integer addressId;

    @CreationTimestamp
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public Integer getId(){
        return this.id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Integer getAddressId(){
        return this.addressId;
    }

    public void setAddressId(Integer addressId){
        this.addressId = addressId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created){
        this.created = created;
    }
}
