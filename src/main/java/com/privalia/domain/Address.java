package com.privalia.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "address")
@EntityListeners(AuditingEntityListener.class)
public class Address implements Serializable
{
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Integer id;

    @NotBlank
    private String address;

    @CreationTimestamp
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public Integer getId(){
        return this.id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getAddress(){
        return this.address;
    }

    public void setAddress(String address){ this.address = address; }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created){
        this.created = created;
    }
}
