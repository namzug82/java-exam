package com.privalia.repositories;

import com.privalia.domain.Student;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource
public interface StudentRepository extends CrudRepository<Student, Integer> {

	@Modifying
	@Transactional
	@Query("UPDATE Student s SET s.name =:name WHERE s.id =:id")
	int updateStudent(@Param("id") int id, @Param("name") String name);

	Student findById(Integer id);

}