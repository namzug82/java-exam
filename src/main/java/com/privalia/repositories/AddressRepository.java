package com.privalia.repositories;

import com.privalia.domain.Address;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource
public interface AddressRepository extends CrudRepository<Address, Integer> {

    @Modifying
    @Transactional
    @Query("UPDATE Address a SET a.address =:address WHERE a.id =:id")
    int updateAddress(@Param("id") int id, @Param("address") String address);

    // Address findById(Integer id);

}