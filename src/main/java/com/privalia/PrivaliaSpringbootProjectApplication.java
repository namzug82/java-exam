package com.privalia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrivaliaSpringbootProjectApplication {
	private final static Logger logger = LoggerFactory.getLogger(PrivaliaSpringbootProjectApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PrivaliaSpringbootProjectApplication.class, args);
		logger.info("info msg");
		logger.debug("debug msg");
	}
}